use std::iter::Peekable;

use pcd_tools::log;
use rand::Rng;

const VECTOR_SIZE: usize = 200_000_000;

fn gen_array(length: usize) -> Vec<i8> {
    let mut rng = rand::thread_rng();

    (0..length).into_iter().map(|_| rng.gen()).collect()
}

struct MergeIterator<T: Ord, I: Iterator<Item = T>> {
    backing_iters: Vec<Peekable<I>>,
}

impl<T: Ord + std::fmt::Debug, I: Iterator<Item = T>> Iterator for MergeIterator<T, I> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        let next = self
            .backing_iters
            .iter_mut()
            .enumerate()
            .filter_map(|(i, x)| Some((i, x.peek()?)))
            .min_by_key(|(_, x)| *x)?;
        let next = next.0;

        self.backing_iters[next].next()
    }
}

/*mod tests {
    #[test]
    fn merge_result_length_is_sum_of_starting_vecs() {
        let mut a = vec![1, 3, 5, 7];
        let b = vec![2, 4, 6];
        super::merge(&mut a, b);
        assert_eq!(7, a.len())
    }

    #[test]
    fn merge_result_is_sorted() {
        let mut a = vec![1, 3, 5, 7];
        let b = vec![2, 4, 6];
        super::merge(&mut a, b);
        let sorted = {
            let mut s = a.clone();
            s.sort();
            s
        };
        assert_eq!(sorted, a)
    }
}*/

fn parallel_sort<T: Ord + Clone + Send + core::fmt::Debug + 'static>(v: Vec<T>) -> Vec<T> {
    MergeIterator {
        backing_iters: v
            .chunks(
                v.len()
                    / std::thread::available_parallelism().unwrap_or(std::num::NonZeroUsize::MIN),
            )
            .map(|x| x.to_vec())
            .enumerate()
            .map(move |(i, mut chunk)| {
                std::thread::spawn(move || {
                    log(&format!("Starting thread #{i}..."));
                    chunk.sort_unstable();
                    log(&format!("Thread #{i} done."));
                    chunk
                })
            })
            .map(|x| {
                x.join()
                    .expect("Worker thread should not panic!")
                    .into_iter()
                    .collect::<Vec<_>>()
                    .into_iter()
                    .peekable()
            })
            .collect(),
    }
    .collect()
}

fn main() {
    log("Generating array...");
    let mut v = gen_array(VECTOR_SIZE);

    log("Array generated.");
    let mut builtin_sorted = v.clone();
    {
        log("Starting sort()");
        let t0 = std::time::Instant::now();
        builtin_sorted.sort_unstable();
        let t1 = std::time::Instant::now();
        log(&format!(
            "sort() done. Time elapsed: {} ms",
            t1.duration_since(t0).as_millis()
        ));
        log(&format!("Sorting {VECTOR_SIZE} elements..."));
    }

    let t0 = std::time::Instant::now();
    v = parallel_sort(v);
    let t1 = std::time::Instant::now();
    log(&format!(
        "Done. Time elapsed: {} ms",
        t1.duration_since(t0).as_millis()
    ));
    log("Starting array comparison");
    assert_eq!(builtin_sorted, v);
    log("Arrays are equal");
}
