pub fn get_unix_time_ms() -> u128 {
    std::time::SystemTime::now().duration_since(std::time::UNIX_EPOCH).unwrap().as_millis()
}

pub fn log(message: &str) {
    println!("[{:?}] {} {}", std::thread::current().name(), get_unix_time_ms(), message);
}
