package pcd.ass01.simenginepar;
public class Mutex {

    private boolean locked = false;
    public Mutex() {
    }

    public synchronized void lock() throws InterruptedException {
        if(this.locked) {
            this.wait();
        }
        this.locked = true;
    }

    public synchronized void unlock() throws InterruptedException {
        if(this.locked) {
            this.locked = false;
            this.notifyAll();
        }
    }

    public synchronized void await() throws InterruptedException {
        if(this.locked) {
            this.wait();
        }
    }
}
