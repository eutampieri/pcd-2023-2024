package pcd.ass01.simenginepar;

import java.util.concurrent.BrokenBarrierException;

public class Barrier {

    final int n;
    int currentCount = 0;
    final Runnable onAllReached;

    public Barrier(int n, Runnable onAllReached) {
        this.n = n;
        this.onAllReached = onAllReached;
    }

    public synchronized void await() throws InterruptedException, BrokenBarrierException {
        this.currentCount += 1;
        if(this.currentCount == this.n) {
            this.onAllReached.run();
            this.currentCount = 0;
            this.notifyAll();
        } else {
            this.wait();
        }
    }

    public synchronized void breakBarrier() {
        this.currentCount = 0;
        this.notifyAll();
    }
}
