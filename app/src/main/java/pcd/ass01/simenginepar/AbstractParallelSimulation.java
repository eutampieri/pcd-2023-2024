package pcd.ass01.simenginepar;

import pcd.ass01.simengineseq_improved.AbstractAgent;
import pcd.ass01.simengineseq_improved.AbstractSimulation;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.stream.IntStream;

public abstract class AbstractParallelSimulation extends AbstractSimulation {

    private class SimulationRunner extends Thread {

        private final long numSteps;
        private boolean running = false;
        private final List<AbstractAgent> agents;
        private final Barrier barrier;
        private final Barrier envReady;

        public SimulationRunner(long numSteps, Barrier barrier, Barrier envReady) {
            this.numSteps = numSteps;
            this.envReady = envReady;
            this.agents = new ArrayList<>();
            this.barrier = barrier;
        }

        public void addAgent(AbstractAgent agent) {
            if(this.running) {
                throw new IllegalStateException("Cannot add an agent while the simulation is running!");
            }
            this.agents.add(agent);
        }
        @Override
        public synchronized void run() {
            this.running = true;
            int nSteps = 0;
            while(nSteps < this.numSteps && this.running) {
                try {
                    this.barrier.await();

                    System.out.println(Thread.currentThread().getName() + ": step " + nSteps);
                    for (var agent: agents) {
                        agent.step(dt);
                    }
                    nSteps += 1;
                    this.envReady.await();
                } catch (InterruptedException | BrokenBarrierException ex) {
                    return;
                }
            }
        }

        public synchronized void stopSimulation() {
            this.running = false;
        }
    }

    private boolean running;

    @Override
    public void run(int numSteps) {
        this.running = true;
        startWallTime = System.currentTimeMillis();
        int numberOfAgents = Runtime.getRuntime().availableProcessors();
        Barrier agentsBarrier = new Barrier(numberOfAgents + 1, () -> {});
        Barrier envReady = new Barrier(numberOfAgents + 1, () -> {});
        List<SimulationRunner> threadPool = IntStream.range(0, numberOfAgents)
                .mapToObj(i -> new SimulationRunner(numSteps, agentsBarrier, envReady))
                .toList();


        /* initialize the env and the agents inside */
        int t = t0;

        env.init();
        for (var a: agents) {
            a.init(env);
        }

        // Assign agents to thread pool
        for(int i = 0; i < agents.size(); i++) {
            threadPool.get(i % threadPool.size()).addAgent(agents.get(i));
        }

        // Start simulation
        for(var r: threadPool) {
            r.start();
        }

        this.notifyReset(t, agents, env);

        long timePerStep = 0;
        int nSteps = 0;

        while (nSteps < numSteps && this.running) {

            currentWallTime = System.currentTimeMillis();

            /* make a step */

            try {
                env.step(dt);
                env.cleanActions();

                System.out.println(Thread.currentThread().getName() + ": step " + nSteps);
                agentsBarrier.await();
                envReady.await();

                t += dt;

                env.processActions();

                notifyNewStep(t, agents, env);

                nSteps++;
                timePerStep += System.currentTimeMillis() - currentWallTime;

                if (toBeInSyncWithWallTime) {
                    syncWithWallTime();
                }
            } catch (InterruptedException | BrokenBarrierException ex) {
                return;
            }
        }

        for (var r : threadPool) {
                agentsBarrier.breakBarrier();
                envReady.breakBarrier();
                r.stopSimulation();
        }

        endWallTime = System.currentTimeMillis();
        this.averageTimePerStep = timePerStep / numSteps;
    }

    public void stop() {
        this.running = false;
    }

    public void reset() {
        this.agents.clear();
    }
}
