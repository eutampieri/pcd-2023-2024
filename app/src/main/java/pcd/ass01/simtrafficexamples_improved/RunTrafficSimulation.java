package pcd.ass01.simtrafficexamples_improved;

/**
 * 
 * Main class to create and run a simulation
 * 
 */
public class RunTrafficSimulation {

	private static int nSteps = 10000;

	public static void main(String[] args) {		

		// var simulation = new TrafficSimulationSingleRoadTwoCars();
		var simulation = new TrafficSimulationSingleRoadSeveralCars();
		// var simulation = new TrafficSimulationSingleRoadWithTrafficLightTwoCars();
		// var simulation = new TrafficSimulationWithCrossRoads();
		simulation.setup();
		
		RoadSimStatistics stat = new RoadSimStatistics();
		RoadSimView view = new RoadSimView(
				() -> new Thread(() -> {
					simulation.reset();
					simulation.setup();
					simulation.run(nSteps);
				}).start(),
                simulation::stop,
				(s) -> {
					nSteps = s;
                    return null;
                }
		);
		view.display();
		
		simulation.addSimulationListener(stat);
		simulation.addSimulationListener(view);		
		simulation.run(nSteps);
	}
}
