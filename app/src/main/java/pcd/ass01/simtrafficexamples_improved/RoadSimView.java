package pcd.ass01.simtrafficexamples_improved;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JPanel;
import pcd.ass01.simengineseq_improved.*;
import pcd.ass01.simtrafficbase_improved.*;

import java.awt.*;
import java.util.function.Function;
import javax.swing.*;

public class RoadSimView extends JFrame implements SimulationListener {

	private RoadSimControls panel;
	private static final int CAR_DRAW_SIZE = 10;
	
	public RoadSimView(Runnable onStart, Runnable onStop, Function<Integer, Void> onStepsChanged) {
		super("RoadSim View");
		setSize(1500,600);

		panel = new RoadSimControls(new RoadSimViewPanel(1500,600), onStart, onStop, onStepsChanged);
		panel.setSize(1500, 600);

		JPanel cp = new JPanel();
		LayoutManager layout = new BorderLayout();
		cp.setLayout(layout);
		cp.add(BorderLayout.CENTER,panel);
		setContentPane(cp);		
		
		setDefaultCloseOperation(EXIT_ON_CLOSE);
			
	}
	
	public void display() {
		SwingUtilities.invokeLater(() -> {
			this.setVisible(true);
		});
	}

	@Override
	public void notifyInit(int t, List<AbstractAgent> agents, AbstractEnvironment env) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void notifyStepDone(int t, List<AbstractAgent> agents, AbstractEnvironment env) {
		var e = ((RoadsEnv) env);
		panel.update(e.getRoads(), e.getAgentInfo(), e.getTrafficLights());
	}

	public class RoadSimControls extends JPanel {
		private class RoadSimControlsPanel extends JPanel {
			private final JButton startButton;
			private final JButton stopButton;

			private final JTextField nSteps;
			private final JButton setStepsButton;

			public RoadSimControlsPanel(Runnable onStart, Runnable onStop, Function<Integer, Void> onStepsChanged) {
				this.setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
				this.startButton = new JButton("Start");
				this.startButton.setAlignmentX(Component.CENTER_ALIGNMENT);
				this.startButton.setEnabled(false);
				this.startButton.addActionListener(e -> {
					startSimulation();
					onStart.run();
				});
				this.stopButton = new JButton("Stop");
				this.stopButton.setAlignmentX(Component.CENTER_ALIGNMENT);
				this.stopButton.setEnabled(true);
				this.stopButton.addActionListener(e -> {
					stopSimulation();
					onStop.run();
				});

				this.nSteps = new JTextField();
				this.setStepsButton = new JButton("Set steps");
				this.setStepsButton.setAlignmentX(Component.CENTER_ALIGNMENT);
				this.setStepsButton.setEnabled(true);
				this.setStepsButton.addActionListener(e -> onStepsChanged.apply(Integer.parseInt(this.nSteps.getText())));

				this.add(this.startButton);
				this.add(this.stopButton);
				this.add(this.nSteps);
				this.add(this.setStepsButton);
			}

			private void startSimulation() {
				this.startButton.setEnabled(false);
				this.stopButton.setEnabled(true);
			}

			private void stopSimulation() {
				this.startButton.setEnabled(true);
				this.stopButton.setEnabled(false);
			}

		}
		final private RoadSimViewPanel panel;

		RoadSimControls(RoadSimViewPanel panel, Runnable onStart, Runnable onStop, Function<Integer, Void> onStepsChanged) {
			this.panel = panel;

			LayoutManager layout = new BorderLayout();
			this.setLayout(layout);
			this.add(BorderLayout.NORTH, new RoadSimControlsPanel(onStart, onStop, onStepsChanged));
			this.add(BorderLayout.SOUTH, this.panel);

		}

		@Override
		public void paintComponent(Graphics g) {
			super.paintComponent(g);
			panel.paintComponent(g);
		}

		public void update(List<Road> roads, List<CarAgentInfo> cars, List<TrafficLight> sems) {
			panel.update(roads, cars, sems);
			this.repaint();
		}

	}

	class RoadSimViewPanel extends JPanel {
		
		List<CarAgentInfo> cars;
		List<Road> roads;
		List<TrafficLight> sems;
		
		public RoadSimViewPanel(int w, int h){
		}

		public void paintComponent(Graphics g) {
			super.paintComponent(g);   
	        Graphics2D g2 = (Graphics2D)g;
			g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
			g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
			g2.clearRect(0,0,this.getWidth(),this.getHeight());
			
			if (roads != null) {
				for (var r: roads) {
					g2.drawLine((int)r.getFrom().x(), (int)r.getFrom().y(), (int)r.getTo().x(), (int)r.getTo().y());
				}
			}
			
			if (sems != null) {
				for (var s: sems) {
					if (s.isGreen()) {
						g.setColor(new Color(11, 95, 11, 255));
					} else if (s.isRed()) {
						g.setColor(new Color(211, 0, 0, 255));
					} else {
						g.setColor(new Color(255, 183, 0, 255));
					}
					g2.fillRect((int)(s.getPos().x()-5), (int)(s.getPos().y()-5), 10, 10);
				}
			}
			
			g.setColor(new Color(0, 0, 0, 255));

			if (cars != null) {
				for (var c: cars) {
					double pos = c.getPos();
					Road r = c.getRoad();
					V2d dir = V2d.makeV2d(r.getFrom(), r.getTo()).getNormalized().mul(pos);
					g2.drawOval((int)(r.getFrom().x() + dir.x() - CAR_DRAW_SIZE/2), (int)(r.getFrom().y() + dir.y() - CAR_DRAW_SIZE/2), CAR_DRAW_SIZE , CAR_DRAW_SIZE);
				}
			}
  	   }
	
	   public void update(List<Road> roads, 
			   			  List<CarAgentInfo> cars,
			   			List<TrafficLight> sems) {
		   this.roads = roads;
		   this.cars = cars;
		   this.sems = sems;
		   repaint();
	   }
	}
}
